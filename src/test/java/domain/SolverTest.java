package domain;

import dto.Edge;
import dto.Face;
import org.junit.Test;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class SolverTest {

    private static Edge edge1 = new Edge(new boolean[]{false, true, false, true, false, false});
    private static Edge edge2 = new Edge(new boolean[]{false, true, true, true, false, false});
    private static Edge edge3 = new Edge(new boolean[]{false, true, false, true, false, true});
    private static Edge edge4 = new Edge(new boolean[]{true, true, false, true, false, false});

    private static Face face1 = Face.newBuilder().withDown(edge1).withLeft(edge1).withRight(edge2).withUp(edge3).build();
    private static Face face2 = Face.newBuilder().withDown(edge2).withLeft(edge1).withRight(edge3).withUp(edge3).build();
    private static Face face3 = Face.newBuilder().withDown(edge1).withLeft(edge1).withRight(edge2).withUp(edge4).build();
    private static Face face4 = Face.newBuilder().withDown(edge2).withLeft(edge1).withRight(edge3).withUp(edge2).build();


    @Test
    public void permute() {
        List<Face> faces = Arrays.asList(face1, face2, face3, face4, face1, face4);
        List<List<Face>> res = new ArrayList<>();

        Solver.permute(faces, 0, res);

        assertEquals(720, res.size());
    }

    @Test
    public void rotationPermute() {
        List<Face> faces = Arrays.asList(face1, face2, face3, face4, face1, face2);
        List<List<Face>> res = new ArrayList<>();

        Solver.rotationPermute(faces, new ArrayList<>(), 0, res);

        assertEquals(4096, res.size());
    }

}