package domain;

import dto.Edge;
import org.junit.Test;
import static org.junit.Assert.*;

public class CheckerTest {
    private static Edge edge1 = new Edge(new boolean[]{false, true, false, true, false, false});
    private static Edge edge2 = new Edge(new boolean[]{false, true, true, true, false, false});
    private static Edge edge3 = new Edge(new boolean[]{true, false, true, false, true, true});

    @Test
    public void isEdgesAdjacent() throws Exception {
        assertFalse(Checker.isEdgesAdjacent(edge1, edge2));
        assertTrue(Checker.isEdgesAdjacent(edge1, edge3));
    }
}