package domain;

import dto.Edge;
import org.junit.Test;

import static org.junit.Assert.*;

public class HelperTest {
    @Test
    public void reverseEdge() throws Exception {
        Edge oldEdge = new Edge(new boolean[]{true, false, false});
        assertArrayEquals(new boolean[]{false, false, true}, Helper.reverseEdge(oldEdge).getEdge());
    }

}