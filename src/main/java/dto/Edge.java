package dto;

import java.util.stream.IntStream;

public class Edge {
    private final boolean[] edge;
    private final int len;

    public Edge(boolean[] edge) {
        this.edge = edge;
        this.len = edge.length;
    }

    public static Edge createEmpty(int len) {
        return new Edge(new boolean[len]);
    }

    public boolean[] getEdge() {
        return edge;
    }

    public int getLen() {
        return len;
    }

    @Override
    public String toString() {
        return IntStream.range(0, len)
                .mapToObj(i -> edge[i] ? "o" : " ")
                .reduce((x, y) -> String.format("%s%s", x, y))
                .orElse("");
    }
}
