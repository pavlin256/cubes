package dto;

import java.util.stream.IntStream;

import static java.lang.String.*;

public class Face {
    private final Edge left;
    private final Edge right;
    private final Edge up;
    private final Edge down;
    private final int len;

    private Face(Builder builder) {
        left = builder.left;
        right = builder.right;
        up = builder.up;
        down = builder.down;
        len = left.getLen();
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public Edge getLeft() {
        return left;
    }

    public Edge getRight() {
        return right;
    }

    public Edge getUp() {
        return up;
    }

    public Edge getDown() {
        return down;
    }

    public int getLen() {
        return len;
    }

    public static final class Builder {
        private Edge left;
        private Edge right;
        private Edge up;
        private Edge down;

        private Builder() {
        }

        public Builder withLeft(Edge val) {
            left = val;
            return this;
        }

        public Builder withRight(Edge val) {
            right = val;
            return this;
        }

        public Builder withUp(Edge val) {
            up = val;
            return this;
        }

        public Builder withDown(Edge val) {
            down = val;
            return this;
        }

        public Face build() {
            return new Face(this);
        }
    }

    public boolean getRightUpCorner() {
        return up.getEdge()[len - 1];
    }

    public boolean getLeftUpCorner() {
        return up.getEdge()[0];
    }

    public boolean getRightDownCorner() {
        return down.getEdge()[len - 1];
    }

    public boolean getLeftDownCorner() {
        return down.getEdge()[0];
    }

    @Override
    public String toString() {
        int len = up.getEdge().length;
        String upStr = up.toString();
        String leftStr = left.toString();
        String rightStr = right.toString();
        String downStr = down.toString();
        String inner = IntStream.range(1, len - 1)
                .mapToObj(i -> "o").reduce("", (x,y) -> format("%s%s",x,y));

        return IntStream.range(1, len - 1)
                .mapToObj(i -> format("%c%s%c", leftStr.charAt(i), inner, rightStr.charAt(i)))
                .reduce(upStr, (x,y) -> format("%s\n%s", x, y))
                .concat(format("\n%s", downStr));
    }
}
