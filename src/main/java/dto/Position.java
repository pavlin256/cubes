package dto;

public enum Position {
    LEFT, RIGHT, UP, DOWN, FRONT, BACK;
}
