package dto;

import domain.Helper;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import static dto.Position.*;
import static java.lang.String.format;

public class Cube {
    private final FixedFace left;
    private final FixedFace right;
    private final FixedFace up;
    private final FixedFace down;
    private final FixedFace front;
    private final FixedFace back;
    private final int len;

    private Cube(Builder builder) {
        left = builder.left;
        right = builder.right;
        up = builder.up;
        down = builder.down;
        front = builder.front;
        back = builder.back;
        this.len = back.face.getLen();
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public FixedFace getLeft() {
        return left;
    }

    public FixedFace getRight() {
        return right;
    }

    public FixedFace getUp() {
        return up;
    }

    public FixedFace getDown() {
        return down;
    }

    public FixedFace getFront() {
        return front;
    }

    public FixedFace getBack() {
        return back;
    }

    public List<FixedFace> getAllFaces() {
        return Arrays.asList(up, down, left, right, front, back);
    }

    public static final class Builder {
        private FixedFace left;
        private FixedFace right;
        private FixedFace up;
        private FixedFace down;
        private FixedFace front;
        private FixedFace back;

        private Builder() {
        }

        public Builder withLeft(Face val) {
            left = new FixedFace(val, LEFT);
            return this;
        }

        public Builder withRight(Face val) {
            right = new FixedFace(val, RIGHT);
            return this;
        }

        public Builder withUp(Face val) {
            up = new FixedFace(val, UP);
            return this;
        }

        public Builder withDown(Face val) {
            down = new FixedFace(val, DOWN);
            return this;
        }

        public Builder withFront(Face val) {
            front = new FixedFace(val, FRONT);
            return this;
        }

        public Builder withBack(Face val) {
            back = new FixedFace(val, BACK);
            return this;
        }

        public Cube build() {
            return new Cube(this);
        }
    }

    public static class FixedFace {
        private final Face face;
        private final Position side;

        private FixedFace(Face face, Position side) {
            this.face = face;
            this.side = side;
        }

        public Face getFace() {
            return face;
        }

        public Position getSide() {
            return side;
        }
    }

    @Override
    public String toString() {
        String emptyFace = Helper.getEmptyFaceRepresentation(len);
        return format("%s%s%s%s",
                getFoldLayer(left.face.toString(), front.face.toString(), right.face.toString()),
                getFoldLayer(emptyFace, down.face.toString(), emptyFace),
                getFoldLayer(emptyFace, back.face.toString(), emptyFace),
                getFoldLayer(emptyFace, up.face.toString(), emptyFace));
    }

    private String getFoldLayer(String left, String middle, String right) {
        String[] l = left.split("\\n");
        String[] m = middle.split("\\n");
        String[] r = right.split("\\n");

        StringBuilder res = new StringBuilder();
        IntStream.range(0, len).forEach(i -> res.append(format("%s%s%s\n", l[i], m[i], r[i])));
        return res.toString();
    }
}
