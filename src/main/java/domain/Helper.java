package domain;

import dto.Edge;
import dto.Face;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class Helper {

    /**
     * Create a new List with swapped elements for given positions
     * */
    public static List<Face> swap(List<Face> original, int i, int j) {
        List<Face> newList = new ArrayList<>(original);
        newList.set(j, original.get(i));
        newList.set(i, original.get(j));
        return newList;
    }

    /**
     * Rotate face for 90 degree clockwise
     * */
    public static Face rotateFace(Face oldFace) {
        return Face.newBuilder()
            .withLeft(oldFace.getDown())
            .withUp(oldFace.getLeft())
            .withRight(oldFace.getUp())
            .withDown(oldFace.getRight())
            .build();
    }


    public static Edge reverseEdge(Edge oldEdge) {
        int len = oldEdge.getEdge().length;
        boolean[] x = new boolean[len];

        IntStream.range(0, len)
                 .forEach(i -> x[i] = oldEdge.getEdge()[len - i - 1]);

        return new Edge(x);
    }

    public static List<Face> updateAcc(List<Face> original, Face newEntry) {
        List<Face> newList = new ArrayList<>(original);
        newList.add(newEntry);
        return newList;
    }

    public static String getEmptyFaceRepresentation(int len) {
        return IntStream.range(0, len)
                .mapToObj(i -> Edge.createEmpty(len).toString())
                .reduce((x,y) -> String.format("%s\n%s", x, y))
                .orElse("");
    }
}
