package domain;

import dto.*;
import java.util.*;
import java.util.function.Function;
import java.util.stream.IntStream;
import static domain.Helper.reverseEdge;
import static dto.Cube.*;
import static dto.Position.*;
import static java.util.Arrays.asList;

public class Checker {

    /**
     * Contains mapping of Face positions on cube to Function that return neighboring Edges by Faces
     * */
    private final static Map<List<Position>, Function<List<Face>, List<Edge>>> adjacentFaceFunByPosition
            = new HashMap<List<Position>, Function<List<Face>, List<Edge>>>()
    {{
        put(asList(LEFT, FRONT), x -> asList(x.get(0).getRight(), x.get(1).getLeft()));
        put(asList(FRONT, RIGHT), x -> asList(x.get(0).getRight(), x.get(1).getLeft()));
        put(asList(FRONT, DOWN), x -> asList(x.get(0).getDown(), x.get(1).getUp()));
        put(asList(DOWN, BACK), x -> asList(x.get(0).getDown(), x.get(1).getUp()));
        put(asList(BACK, UP), x -> asList(x.get(0).getDown(), x.get(1).getUp()));
        put(asList(LEFT, DOWN), x -> asList(reverseEdge(x.get(0).getDown()), x.get(1).getLeft()));
        put(asList(RIGHT, DOWN), x -> asList(x.get(0).getDown(), x.get(1).getRight()));
        put(asList(LEFT, BACK), x -> asList((reverseEdge(x.get(0).getLeft())), x.get(1).getLeft()));
        put(asList(RIGHT, BACK), x -> asList(reverseEdge(x.get(0).getRight()), x.get(1).getRight()));
        put(asList(FRONT, UP), x -> asList(x.get(0).getUp(), x.get(1).getDown()));
        put(asList(RIGHT, UP), x -> asList(reverseEdge(x.get(0).getUp()), x.get(1).getRight()));
        put(asList(LEFT, UP), x -> asList(x.get(0).getUp(), x.get(1).getLeft()));
    }};

    /**
     * Contains mapping of Face positions on cube to Function that return neighboring Corners by Faces
     * */
    private final static Map<List<Position>, Function<List<Face>, Boolean>> adjacentFaceFunByCorner
            = new HashMap<List<Position>, Function<List<Face>, Boolean>>()
    {{
        put(asList(LEFT, FRONT, UP), x -> isCornersAdjacent(asList(x.get(0).getRightUpCorner(), x.get(1).getLeftUpCorner(), x.get(2).getLeftDownCorner())));
        put(asList(LEFT, FRONT, DOWN), x -> isCornersAdjacent(asList(x.get(0).getRightDownCorner(), x.get(1).getLeftDownCorner(), x.get(2).getLeftUpCorner())));
        put(asList(FRONT, DOWN, RIGHT), x -> isCornersAdjacent(asList(x.get(0).getRightDownCorner(), x.get(1).getRightUpCorner(), x.get(2).getLeftDownCorner())));
        put(asList(FRONT, RIGHT, UP), x -> isCornersAdjacent(asList(x.get(0).getRightUpCorner(), x.get(1).getLeftUpCorner(), x.get(2).getRightDownCorner())));
        put(asList(LEFT, DOWN, BACK), x -> isCornersAdjacent(asList(x.get(0).getLeftDownCorner(), x.get(1).getLeftDownCorner(), x.get(2).getLeftUpCorner())));
        put(asList(DOWN, BACK, RIGHT), x -> isCornersAdjacent(asList(x.get(0).getRightDownCorner(), x.get(1).getRightUpCorner(), x.get(2).getRightDownCorner())));
        put(asList(BACK, UP, LEFT), x -> isCornersAdjacent(asList(x.get(0).getLeftDownCorner(), x.get(1).getLeftUpCorner(), x.get(2).getLeftUpCorner())));
        put(asList(BACK, UP, RIGHT), x -> isCornersAdjacent(asList(x.get(0).getRightDownCorner(), x.get(1).getRightUpCorner(), x.get(2).getRightUpCorner())));
    }};


    /**
     * Check if cube correct by checking if the neighboring corners and edges could be joined
     * */
    public static boolean isCorrectCube(Cube cube) {
        List<FixedFace> allFaces = cube.getAllFaces();
        for (FixedFace face1: allFaces) {
            for (FixedFace face2: allFaces) {
                if (!isFacesAdjacentBySides(face1, face2)) return false;
                for (FixedFace face3: allFaces) {
                    if (!isFacesAdjacentByCorners(face1, face2, face3)) return false;
                }
            }
        }
        return true;
    }

    /**
     * Check if the neighboring corner of given faces could be joined.
     * If these faces have not neighboring corner, return true.
     * */
    private static boolean isFacesAdjacentByCorners(FixedFace face1, FixedFace face2, FixedFace face3) {
        Function<List<Face>, Boolean> fun = adjacentFaceFunByCorner.get(asList(face1.getSide(), face2.getSide(), face3.getSide()));
        if (fun == null) return true;

        return fun.apply(asList(face1.getFace(), face2.getFace(), face3.getFace()));
    }

    /**
     * Check if the neighboring edges of given faces could be joined.
     * If these faces have not neighboring edges, return true.
     * */
    private static boolean isFacesAdjacentBySides(FixedFace face1, FixedFace face2) {
        Function<List<Face>, List<Edge>> fun = adjacentFaceFunByPosition.get(asList(face1.getSide(), face2.getSide()));
        if (fun == null) return true;
        List<Edge> p = fun.apply(asList(face1.getFace(), face2.getFace()));
        return isEdgesAdjacent(p.get(0), p.get(1));
    }

    static boolean isEdgesAdjacent(Edge first, Edge second) {
        return IntStream.range(1, first.getEdge().length - 1)
                .filter(i -> (first.getEdge()[i] == second.getEdge()[i]))
                .count() == 0;
    }

    private static boolean isCornersAdjacent(List<Boolean> corner) {
        return corner.stream().mapToInt(i -> i ? 1 : 0).sum() == 1;
    }
}
