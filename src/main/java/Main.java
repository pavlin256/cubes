import domain.Checker;
import domain.Solver;
import dto.Cube;
import dto.Edge;
import dto.Face;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Edge edge1 = new Edge(new boolean[]{false, false, true, false, false});
        Edge edge2 = new Edge(new boolean[]{true, false, true, false, true});
        Edge edge3 = new Edge(new boolean[]{true, true, false, true, true});
        Edge edge4 = new Edge(new boolean[]{false, true, false, true, false});
        Edge edge5 = new Edge(new boolean[]{true, true, false, true, false});
        Edge edge6 = new Edge(new boolean[]{false, true, false, true, true});
        Edge edge7 = new Edge(new boolean[]{true, false, true, false, false});
        Edge edge8 = new Edge(new boolean[]{false, false, true, false, true});

        Face face1 = Face.newBuilder().withDown(edge1).withLeft(edge1).withRight(edge1).withUp(edge1).build();
        Face face2 = Face.newBuilder().withDown(edge2).withLeft(edge3).withRight(edge3).withUp(edge2).build();
        Face face3 = Face.newBuilder().withDown(edge1).withLeft(edge1).withRight(edge4).withUp(edge1).build();
        Face face4 = Face.newBuilder().withDown(edge5).withLeft(edge6).withRight(edge1).withUp(edge4).build();
        Face face5 = Face.newBuilder().withDown(edge7).withLeft(edge6).withRight(edge4).withUp(edge4).build();
        Face face6 = Face.newBuilder().withDown(edge3).withLeft(edge8).withRight(edge6).withUp(edge4).build();

        Cube cube = Solver.solveCubePuzzle(Arrays.asList(face1, face2, face3, face4, face5, face6)).get();

        Checker.isCorrectCube(cube);

        System.out.print(cube);
    }
}
